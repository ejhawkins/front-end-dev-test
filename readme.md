# AlgaeCal —— Front-end Development Test

Landing page for AlgaeCal, a company that guarentees stronger bones or your money back. Interactive landing page by AlgaeCal fit to answer all the questions the potential AlgaeCal customer may have. There were a list of fixes that needed to be made and each one was addressed. Not only does AlgaeCal guarentee strong bones, they also guarentee strong code!
----

[image]

## Installation

Clone this respository or download the folder. Use the following command line.
git clone https://ejhawkins@bitbucket.org/ejhawkins/front-end-dev-test.git

Locate the file you wish to clone.
cd front-end-dev-test
<img src="https://ibb.co/sFPdkjP">

Initialize the file directory.
git init (AlgaeCal prefers a repository you can commit to)

Create a gulp file.
There should already be a gulpfile.js inside the repository that you downloaded.

Install gulp sass, if you wish to edit the SASS file as CSS.
npm install gulp-sass 
npm install --save-dev gulp

Install all node modules and dependencies. Make sure you have node.js installed.
npm install

To run in production.
npm start
npm run serve (In seperate terminal)

To run in development environment with source files 
npm run start:dev

To compile the files together 
Make all edits and coding in the development in environment (./src/)
npm run build
All files should be transported to production (./dist/)
<img src="https://ibb.co/6DdMMxK">

## Features

Load video play button that plays when you click the button. 
<img src="https://ibb.co/MDW0Cyy">

Modal that loads directly from the API. 

Percentage bubbles that appear only when a percentage is taken off. 
<img src="https://ibb.co/dWTYS44">

Hours of operations and when is the right time to contact a bone specialist.

----
## changelog (ideas)


