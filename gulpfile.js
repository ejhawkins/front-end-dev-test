const gulp = require('gulp');
const sass = require('gulp-sass');

// Testing the environment to see that something is returned into the console.
gulp.task('default', () => {
  console.log('Hello AlgaeCal Team');
});
// Returns SASS files as CSS files.
gulp.task('styles', () => {
  gulp
    .src('sass/**/*.scss')
    .pipe(sass())
    .on('error', sass.logError)
    .pipe(gulp.dest('./css'));
});
