// import {ProductBundles} from "./modules/ProductBundles"; TO DO 
import './styles/main.scss';
import videoInterface from './modules/videoplayer';
import fetchAPI from './modules/options';
import loadSVGs from './modules/svg-replace';
import 'popper.js';
import 'bootstrap';
import jstz from 'jstz';
import { moment } from './modules/moment';
import momentHoliday from './modules/moment-holidays';


// Bootstrap collapse 
//$('.card-header').click(function () {
//	this.collapse('hide');	
//})


// Modal Script
// First let's grab the data from the api. Since 7 reads as an error, a different approach was used to get the data. 
const api = fetchAPI[0].acf['7yr_full_copy'];
// Find the place of the modal that needs content. The title was in the incorrect error (meaning not similiar to the other modals with the title in the header).
const title = document.getElementById('modal').getElementsByTagName('H3');
// Grab the Guarantee Modal, the one for see more details.
const guaranteeModal = document.getElementById('modal');
// Select the first <h3> heading for the title.
const headingThree = guaranteeModal.getElementsByTagName('H3');
// Place the awesome text inside the modal. 
document.getElementById('modal').innerHTML = api;
// Place the title in the correct area for a consistent feel.
document.getElementById('modal-title-guarentee').innerHTML = headingThree[0].innerText;
// Function to delete and replace the first <h3> header.
function eraseTitle() {
	const another = document.querySelector('#modal h3');
	another.classList.add('hide');
}
eraseTitle();
// The image was not fluid and cannot be accessed only through the api. This function makes the image responsive to the screen and modal. 
function fluidImage() {
	const image = document.querySelector('.modal img');
	image.classList.add('img-fluid');
}
fluidImage();

// Time & Date Scripts 

// Configuring time zone for the current area.
const timezone = jstz.determine() || 'UTC';
// Gather current time to be compared with office hours.
const currentTime = moment().format('HH:mm');
// Grab the current date. Must be in word format like "Tuesday" for example.
var currentDate = moment().format('dddd');

// Office Hours 
// This is a variable for the date Monday
var monday = moment().isoWeekday(1).format('dddd');
console.log(monday)

// Conditional statement that determines the day and time. Afterwards it checks to see if the office is open, closed or it's too early
if (currentDate === monday){
// Grab the times for the JSON file (fetchAPI[0].starting_time). Can be used for an API too with the fetch method. Compares current time to opening hours. You can add an opening soon feature if you would like. 
     if (currentTime < fetchAPI[0].acf.office_hours[0].starting_time){
// Of course first we need to grab the div container holding the "Speak to our Bone Health Consultants" text. 
        var div = document.getElementById("speak_to_our_bone_specialists");
// Vertical-wipe basically means it will be hidden until hours of operation.
        div.classList.add("vertical-wipe");
// This is a message just for development area not production. 
        console.log("Too Early")
        } 
// Compare current time to closing time, as with opening soon you can have an closing soon message appear for the users. 
	    else if(currentTime > fetchAPI[0].acf.office_hours[0].closing_time){
	        var div = document.getElementById("speak_to_our_bone_specialists");
	        div.classList.add("vertical-wipe");
	        console.log("Closed")
	    } 
// If it is not too early in time or too late in time it will be open matching office hours.
	    else{        
	        var div = document.getElementById("speak_to_our_bone_specialists");
	        div.classList.remove("vertical-wipe");
	        console.log("Open");
	    }
	}
	else{
// Another dev comment stating that today isn't Monday basically. 
     		console.log("Not the current date")    
}

// What is special about these function is that the hours vary on the day. 
var tuesday = moment().isoWeekday(2).format('dddd');

if (currentDate === tuesday){
  		   if (currentTime < fetchAPI[0].acf.office_hours[1].starting_time){
        var div = document.getElementById("speak_to_our_bone_specialists");
        div.classList.add("vertical-wipe");
        console.log("Too Early")
        } 
	    else if(currentTime > fetchAPI[0].acf.office_hours[1].closing_time){
	        var div = document.getElementById("speak_to_our_bone_specialists");
	        div.classList.add("vertical-wipe");
	        console.log("Closed")
	    } 
	    else{        
	        var div = document.getElementById("speak_to_our_bone_specialists");
	        div.classList.remove("vertical-wipe");
	        console.log("Open");
	    }
	}
	else{
     		console.log("Not the current date")    
}

var wednesday = moment().isoWeekday(3).format('dddd');

if (currentDate === wednesday){
  		   if (currentTime < fetchAPI[0].acf.office_hours[2].starting_time){
        var div = document.getElementById("speak_to_our_bone_specialists");
        div.classList.add("vertical-wipe");
        console.log("Too Early")
        } 
	    else if(currentTime > fetchAPI[0].acf.office_hours[2].closing_time){
	        var div = document.getElementById("speak_to_our_bone_specialists");
	        div.classList.add("vertical-wipe");
	        console.log("Closed")
	    } 
	    else{        
	        var div = document.getElementById("speak_to_our_bone_specialists");
	        div.classList.remove("vertical-wipe");
	        console.log("Open");
	    }
	}
	else{
     		console.log("Not the current date")    
}

var thursday = moment().isoWeekday(4).format('dddd');

if (currentDate === wednesday){
  		   if (currentTime < fetchAPI[0].acf.office_hours[3].starting_time){
        var div = document.getElementById("speak_to_our_bone_specialists");
        div.classList.add("vertical-wipe");
        console.log("Too Early")
        } 
	    else if(currentTime > fetchAPI[0].acf.office_hours[3].closing_time){
	        var div = document.getElementById("speak_to_our_bone_specialists");
	        div.classList.add("vertical-wipe");
	        console.log("Closed")
	    } 
	    else{        
	        var div = document.getElementById("speak_to_our_bone_specialists");
	        div.classList.remove("vertical-wipe");
	        console.log("Open");
	    }
	}
	else{
     		console.log("Not the current date")    
}

var friday = moment().isoWeekday(5).format('dddd');

if (currentDate === friday){
  		   if (currentTime < fetchAPI[0].acf.office_hours[4].starting_time){
        var div = document.getElementById("speak_to_our_bone_specialists");
        div.classList.add("vertical-wipe");
        console.log("Too Early")
        } 
	    else if(currentTime > fetchAPI[0].acf.office_hours[4].closing_time){
	        var div = document.getElementById("speak_to_our_bone_specialists");
	        div.classList.add("vertical-wipe");
	        console.log("Closed")
	    } 
	    else{        
	        var div = document.getElementById("speak_to_our_bone_specialists");
	        div.classList.remove("vertical-wipe");
	        console.log("Open");
	    }
	}
	else{
     		console.log("Not the current date")    
}

var saturday = moment().isoWeekday(6).format('dddd');

if (currentDate === saturday){
  		   if (currentTime < fetchAPI[0].acf.office_hours[5].starting_time){
        var div = document.getElementById("speak_to_our_bone_specialists");
        div.classList.add("vertical-wipe");
        console.log("Too Early")
        } 
	    else if(currentTime > fetchAPI[0].acf.office_hours[5].closing_time){
	        var div = document.getElementById("speak_to_our_bone_specialists");
	        div.classList.add("vertical-wipe");
	        console.log("Closed")
	    } 
	    else{        
	        var div = document.getElementById("speak_to_our_bone_specialists");
	        div.classList.remove("vertical-wipe");
	        console.log("Open");
	    }
	}
	else{
     		console.log("Not the current date")    
}

var sunday = moment().isoWeekday(7).format('dddd');

if (currentDate === saturday){
  		   if (currentTime < fetchAPI[0].acf.office_hours[6].starting_time){
        var div = document.getElementById("speak_to_our_bone_specialists");
        div.classList.add("vertical-wipe");
        console.log("Too Early")
        } 
	    else if(currentTime > fetchAPI[0].acf.office_hours[6].closing_time){
	        var div = document.getElementById("speak_to_our_bone_specialists");
	        div.classList.add("vertical-wipe");
	        console.log("Closed")
	    } 
	    else{        
	        var div = document.getElementById("speak_to_our_bone_specialists");
	        div.classList.remove("vertical-wipe");
	        console.log("Open");
	    }
	}
	else{
     		console.log("Not the current date")    
}

// Above and Beyond* Closing Soon for Monday (Sample)
function closingSoonWeekly(){
// "Closing Soon" will show when it gets close to closing time therefore future customers get a heads up and are not disappointed if they missed office hours. You can test this by entering a time integer in military or UTC time. 
    if (currentTime < 1600 && currentTime > 1530){
// Grabing the div added for "closing soon" text   
    var closeSoon = document.getElementById("closing_soon");
//  Simple function to hide and show the div and not delete it.
    closeSoon.classList.remove("hide");
    closeSoon.classList.add('show-div');
    }else{
    	console.log("Office is closed")
    }
} 
// Call the function to display the "Closing Soon" text. This is an extra function. 
closingSoonWeekly();

// Above and Beyond* Holiday Date Exclusion. We taking out office hours but what about holidays? 

function checkIsHoliday(date) {
  const _holidays = {
    M: {
// Month, Day
      '01/01': "New Year's Day",
      '07/04': 'Independence Day',
      '11/11': "Veteran's Day",
      '11/28': 'Thanksgiving Day',
      '11/29': 'Day after Thanksgiving',
      '12/24': 'Christmas Eve',
      '12/25': 'Christmas Day',
      '12/31': "New Year's Eve",
    },
    W: {
// Month, Week of Month, Day of Week
      '1/3/1': 'Martin Luther King Jr. Day',
      '2/3/1': "Washington's Birthday",
      '5/5/1': 'Memorial Day',
      '9/1/1': 'Labor Day',
      '10/2/1': 'Columbus Day',
      '11/4/4': 'Thanksgiving Day',
    },
  };

// Determine the dates
  const diff = 1 + (0 | (new Date(date).getDate() - 1) / 7);
  const memorial = (new Date(date).getDay() === 1 && (new Date(date).getDate() + 7) > 30) ? '5' : null;
  var myObject = _holidays.M;

  var myObject = _holidays;

  const currentDay = moment().format('MM/DD');

// Holiday Day-Off Validation, it's validating if today is a holiday and whether to show the "Speak to our Bone Health Consultants" text.
  let validate = moment(currentDay).isHoliday('Christmas');

// If true (It's a holiday) a bonespecialist will not be available.
  if (validate === true) {
    var div = document.getElementById('speak_to_our_bone_specialists');
    div.classList.add('vertical-wipe');
  }
// If false (It's not a holiday) a bonespecialist will be available depending on business hours
  if (validate = false) {
    var div = document.getElementById('speak_to_our_bone_specialists');
    div.classList.remove('vertical-wipe');
    console.log('Have a good day!');
  }
// Returns an object with the values from the holiday js object with each key mapped using mapFn(value).
  function objectMap(object, mapFn) {
    return Object.keys(object).reduce((result, key) => {
      result[key] = mapFn(object[key]);
      return result;
    }, {});
  }

  const newMap = objectMap(myObject, value => value * 2);
  // Test listing
  console.log(`map${newMap}`);

  return (_holidays.M[moment(date).format('MM/DD')] || _holidays.W[moment(date).format(`M/${memorial || diff}/d`)]);
}
// Call the function
checkIsHoliday();



document.addEventListener('DOMContentLoaded', () => {
  // Load SVGS
  loadSVGs();
  // Video Script
  videoInterface();
  // Holiday Script
  momentHoliday();
});



