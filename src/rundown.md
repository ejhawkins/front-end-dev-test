⬥ AlgaeCal —— Front-end Development Test

  ╭── <Rundown/(TO DO)>
  │
☐ ├── ⟐ README file
☐ │   ├── Clear formatting
☐ │   ├── Neat organization
☐ │   ├── Easy-to-follow instructions
☐ │   ╰── Image of Final Page
☐ ├── ⟐ Instructions
☐ │   ├── Clone git respository
☐ │   ├── Install Dependencies in Repository
☐ │   ├── Install Gulp Globally
☐ │   ├── Create a directory for repo
☐ │   ├── Create a package.json file
☐ │   ├── Install the gulp package dependencies
☐ │   ├── Create gulpfile
☐ │   ├── Install gulp-sass
☐ │   ├── Create a new folder called SASS for .scss
☐ │   ├── Move all CSS files to a folder
☐ │   ├── Move all SCSS files to a folder* (Might delete)
☐ │   ├── Change the extensions from css to scss
☐ │   ├── Insert gulp-sass in gulp file 
☐ │   ├── Tell Gulp which files you want to work with
☐ │   ├── Write `gulp styles` in terminal as needed* 
☐ │   ├── Add scss error protection say it doesn't work
☐ │   ╰── Start The Server
☐ ├── ⟐ Task 1 
☐ │   ├── ⬦ The 3rd product bundle doesn't match the other two. It should have the blue box around it, but the prices are not formatted correctly. 
☐ │   │   ├── Fix the blue box around the item by adding the class name `.purchase-options-new-cta"`
☐ │   │   ╰── Fix the pricing format on for the item with "X". /* Check the class name again (delete)*/
☐ │   ├── ⬦ Also "You Save $x" shoud be Red (#FF0000) for all pricing bundles
☐ │   ├── ⬦ The dollar sign is missing in multiple places
☐ │   │   ├── Item 1 
☐ │   │   ├── Item 2
☐ │   │   ╰── Item 3
☐ │   ├── ⬦ The color of the "Most Popular" wrapper shoudl be #019ea
☐ │   ├── ⬦ The % off bubble is in the wrong place
☐ │   ├── ⬦The video doesn't play when you click on the image. 
☐ │   │   ├── Find the code
☐ │   │   ├── Add documentation
☐ │   │   ╰── UX Bonus: Add a pause function to the video and a subscribing function
☐ │   ╰──⬦ % off Bubble
☐ │       ├── Create a javascript function to show when it is more than 0% off if not hide the bubbl
☐ │       ╰── UX Bonus: Add an animation to bubble
☐ ├── ⟐ Task 2 
☐ │   ├── ⬦ Use an ajax request to populate this modal. 
☐ │   │   ├── Create a bootstrap modal
☐ │   │   ├──  Load content from API
☐ │   │   ├──  Add a link for a video
☐ │   │   ├──  Load content from API
☐ │   │   ╰── Add video with same functionality as the one on the homepage
☐ ├── ⟐ Task 3
☐ │   ├── ⬦ Business Hour Indicator
☐ │   │   ├── Create a javascript function for existing business hours.
☐ │   │   ├── Create a javascript function for time zones.
☐ │   │   ╰── Crearte a javascription function to hide during non-business hours.
☐ ├── ⟐ UI Testing  & Unit Testing w/ Jasmine
☐ │   ├── ⬦ Browser Testing
☐ │   │   ├── Safari
☐ │   │   ├── Chrome 
☐ │   │   ├── IE
☐ │   │   ╰── Firefox
☐ │   ├── ⬦ UI Testing
☐ │   │   ├── Customer must be able to read pricing for items clearly.
☐ │   │   ├── Customer must be able to clearly understand when they can talk to a consultant.
☐ │   │   ╰── Customer must be able to view videos to learn more about the brand overall as a company.
☐ │   ╰── ⬦ Unit Testing
☐ │       ├── Video player test. Video plays and pauses on command.
☐ │       ├── Business indicator hides the speak to a consultant text
☐ │       ├── Time is correct for business indicator.
☐ │       ├── 0% hidden when the percentage doesn't count.
☐ │       ├── Modal loads on click.
☐ │       ╰── Business indicator uses time zones correctly.
☐ ├── ⟐ Code Functionality
☐ │   ├── ⬦ CSS
☐ │   │   ├── Converts to SCSS error-free
☐ │   │   ╰── Organized code
☐ │   ├── ⬦ HTML
☐ │   │   ├── Indented correctly
☐ │   │   ╰── All the scripts work
☐ │   ╰── ⬦ JavaScript
☐ │       ├── Refactored to the simpliest form that is easy to edit by fellow developers.
☐ │       ├── Formatted correctly
☐ │       ╰── Error-free to standard JS
☐ ├── ⟐ Project Submitted (2 HOURS MAX[45 MINUTES])
  │
  ╰── </Rundown/(TO DO)>

  ╭── <AboveAndBeyond/(ADDIONTAL THINGS THAT CAN BE DONE)>
  │
☐ ├── ⟐ ServiceWorker
☐ │   ├── Install a servive worker
☐ │   ├── Register the service worker
☐ │   ╰── Build a cache called 'algaecal-cache-v1'
☐ ├── ⟐  GIF
☐ │   ├── Design some mock ups for gifs and find places where they can be used. 
☐ │   ╰── Build in Adobe After Effects, Adobe Photoshop and Adobe Illustrator.
☐ ├── ⟐  Make Red Slashes
☐ │   ├── Build the strike in SCSS
☐ │   ╰── Create a javascript function that selects "retail-price" class containing divs and adds a slash over them.
☐ ├── ⟐  Holidays
☐ │   ├── 
☐ │   ╰── 
☐ ├── ⟐  Closing Soon
☐ │   ├── Install/import moment-holiday javaScript file
☐ │   ├── Build a function that can verify dates 
☐ │   ╰── Create div box for "closing soon" and "opening soon" text.
☐ ├── ⟐  Jasmine Testing Environment
☐ │   ├── Create a specRunner file
☐ │   ├── Build HTML and JS for testing
☐ │   ╰── ⬦ Make tests for the following: 
☐ │       ├── Video player test. Video plays and pauses on command.
☐ │       ├── Business indicator hides the speak to a consultant text
☐ │       ├── Time is correct for business indicator.
☐ │       ├── 0% hidden when the percentage doesn't count.
☐ │       ├── Modal loads on click.
☐ │       ╰── Business indicator uses time zones correctly.
  │
  ╰── </AboveAndBeyond>

To mark a task complete, copy the checkbox glyph below & 
paste it over the unchecked one next to your completed task.

Checked Box: ☑︎